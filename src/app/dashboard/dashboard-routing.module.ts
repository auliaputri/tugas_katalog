import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboradComponent } from './dashborad/dashborad.component';
import { PromoComponent } from '../layout/promo/promo.component';
import { LaporanComponent } from '../layout/laporan/laporan.component';
import { UsermanagementComponent } from '../layout/usermanagement/usermanagement.component';
import { BarangsewaComponent } from '../layout/barangsewa/barangsewa.component';
import { PemesananComponent } from '../layout/pemesanan/pemesanan.component';


const routes: Routes = [
  {
    path: 'dashboard', component: DashboradComponent,
    children: [
      {
        path: 'katalog', loadChildren: '../layout/katalog/katalog.module#KatalogModule',
      },
      {
        path: 'usermanagement', loadChildren: '../layout/usermanagement/usermanagement.module#UserModule',
      },
      {
        path: 'barangsewa', loadChildren: '../layout/barangsewa/barangsewa.module#BarangModule',
      },
      {
        path: 'pemesanan', loadChildren: '../layout/pemesanan/pemesanan.module#PemesananModule',
      },
      {
        path: 'pembayaran', loadChildren: '../layout/pembayaran/pembayaran.module#PembayaranModule',
      },
      {
        path: 'promo', loadChildren: '../layout/promo/promo.module#PromoModule',
      },
      {
        path: 'laporan', loadChildren: '../layout/laporan/laporan.module#LaporanModule',
      },

    ],
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
