import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
/*Custom Files*/
import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ForgotComponent } from './forgot/forgot.component';

@NgModule({

  imports: [
    CommonModule,
    LoginRoutingModule
  ],

  declarations: [
  	LoginComponent, 
  	RegisterComponent, 
  	ForgotComponent]

})
export class LoginModule { }
