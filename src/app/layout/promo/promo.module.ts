import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { PromoComponent } from './promo.component';


@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild([

            { path: '', component: PromoComponent },
        ]),
    ],
    declarations: [

        PromoComponent,

    ],
})
export class PromoModule { }
