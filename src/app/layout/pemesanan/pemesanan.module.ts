import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { PemesananComponent } from './pemesanan.component';


@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild([

            { path: '', component: PemesananComponent },
        ]),
    ],
    declarations: [

        PemesananComponent,

    ],
})
export class PemesananModule { }
