import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BarangmasukComponent } from './barangmasuk.component';

describe('BarangmasukComponent', () => {
  let component: BarangmasukComponent;
  let fixture: ComponentFixture<BarangmasukComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BarangmasukComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BarangmasukComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
