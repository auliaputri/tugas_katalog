import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { BarangsewaComponent } from './barangsewa.component';
import { BarangmasukComponent } from './barangmasuk/barangmasuk.component';
import { BarangkeluarComponent } from './barangkeluar/barangkeluar.component';


@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild([

            { path: '', component: BarangsewaComponent },
            { path: 'barangmasuk', component: BarangmasukComponent },
            { path: 'barangkeluar', component: BarangkeluarComponent },
        ]),
    ],
    declarations: [

        BarangsewaComponent,

        BarangmasukComponent,

        BarangkeluarComponent,

    ],
})
export class BarangModule { }
