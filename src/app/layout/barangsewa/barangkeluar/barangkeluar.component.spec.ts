import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BarangkeluarComponent } from './barangkeluar.component';

describe('BarangkeluarComponent', () => {
  let component: BarangkeluarComponent;
  let fixture: ComponentFixture<BarangkeluarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BarangkeluarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BarangkeluarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
