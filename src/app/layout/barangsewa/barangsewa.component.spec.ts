import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BarangsewaComponent } from './barangsewa.component';

describe('BarangsewaComponent', () => {
  let component: BarangsewaComponent;
  let fixture: ComponentFixture<BarangsewaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BarangsewaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BarangsewaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
