import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { KatalogComponent } from './katalog.component';
import { CreateComponent } from './create/create.component';
import { ProdukterbaruComponent } from './produkterbaru/produkterbaru.component';
import { ProdukpopulerComponent } from './produkpopuler/produkpopuler.component';
import { MotorComponent } from './motor/motor.component';
import { MobilComponent } from './mobil/mobil.component';
import { SepedaComponent } from './sepeda/sepeda.component';
import { FashionComponent } from './fashion/fashion.component';
import { DekorasiComponent } from './dekorasi/dekorasi.component';
import { OutdoorComponent } from './outdoor/outdoor.component';
import { ElektronikComponent } from './elektronik/elektronik.component';
import { FotografiComponent } from './fotografi/fotografi.component';
import { ArtComponent } from './art/art.component';


@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild([

            { path: '', component: KatalogComponent },
            { path: 'create', component: CreateComponent },
            { path: 'produkterbaru', component: ProdukterbaruComponent },
            { path: 'produkpopuler', component: ProdukpopulerComponent },
            { path: 'motor', component: MotorComponent },
            { path: 'mobil', component: MobilComponent },
            { path: 'sepeda', component: SepedaComponent },
            { path: 'fashion', component: FashionComponent },
            { path: 'dekorasi', component: DekorasiComponent },
            { path: 'outdoor', component: OutdoorComponent },
            { path: 'elektronik', component: ElektronikComponent },
            { path: 'fotografi', component: FotografiComponent },
            { path: 'art', component: ArtComponent },
        ]),
    ],
    declarations: [

        KatalogComponent,

        CreateComponent,

        ProdukterbaruComponent,

        ProdukpopulerComponent,

        MotorComponent,

        MobilComponent,

        SepedaComponent,

        FashionComponent,

        DekorasiComponent,

        OutdoorComponent,

        ElektronikComponent,

        FotografiComponent,

        ArtComponent,

    ],
})
export class KatalogModule { }
