import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DekorasiComponent } from './dekorasi.component';

describe('DekorasiComponent', () => {
  let component: DekorasiComponent;
  let fixture: ComponentFixture<DekorasiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DekorasiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DekorasiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
