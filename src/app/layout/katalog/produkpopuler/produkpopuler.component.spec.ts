import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProdukpopulerComponent } from './produkpopuler.component';

describe('ProdukpopulerComponent', () => {
  let component: ProdukpopulerComponent;
  let fixture: ComponentFixture<ProdukpopulerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProdukpopulerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProdukpopulerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
