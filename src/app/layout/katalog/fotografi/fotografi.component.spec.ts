import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FotografiComponent } from './fotografi.component';

describe('FotografiComponent', () => {
  let component: FotografiComponent;
  let fixture: ComponentFixture<FotografiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FotografiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FotografiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
