import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SepedaComponent } from './sepeda.component';

describe('SepedaComponent', () => {
  let component: SepedaComponent;
  let fixture: ComponentFixture<SepedaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SepedaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SepedaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
