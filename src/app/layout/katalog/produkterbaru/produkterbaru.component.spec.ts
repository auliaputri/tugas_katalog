import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProdukterbaruComponent } from './produkterbaru.component';

describe('ProdukterbaruComponent', () => {
  let component: ProdukterbaruComponent;
  let fixture: ComponentFixture<ProdukterbaruComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProdukterbaruComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProdukterbaruComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
