import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { LaporanComponent } from './laporan.component';


@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild([

            { path: '', component: LaporanComponent },
        ]),
    ],
    declarations: [

        LaporanComponent,

    ],
})
export class LaporanModule { }
